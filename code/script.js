// Напиши функцію map(fn, array), яка приймає на вхід функцію та масив, та обробляє кожен елемент масиву цією функцією, повертаючи новий масив.

alert(
  `Завдання 1. Маємо масив [1, 2, 3, 4, 6, 8]. За допомогою функції map змінимо даний масив, збільшивши кожен його елемент на 10.`
);
let arr = [1, 2, 3, 4, 6, 8];

let cbFunc = (x, y) => {
  return (y[x] += 10);
};

function map(fn, array) {
  for (let i = 0; i < array.length; i++) {
    fn(i, array);
  }
  // document.write(arr);
  alert(`Змінений масив: [${arr}].`);
}

map(cbFunc, arr);

// Використовуючи CallBack function, створіть калькулятор, який буде від користувача приймати 2 числа і знак арифметичної операції. При введенні не числа або при розподілі на 0 виводити помилку.
alert("Завдання 2. Примітивний калькулятор.");

let num1, num2, aSign;

let checkNum1 = () => {
  num1 = parseInt(prompt("Введіть перше число:"));
  if (isNaN(num1)) {
    alert("Введені дані не є числом!");
    checkNum1();
  }
  return num1;
};

let checkNum2 = () => {
  num2 = parseInt(prompt("Введіть друге число:"));
  if (isNaN(num2)) {
    alert("Введені дані не є числом!");
    checkNum2();
  }
  return num2;
};

let checkSign = () => {
  aSign = prompt("Введіть знак арифметичної дії:");
  if (/[^\+\-\*\/]/.test(aSign)) {
    alert("Введіть один з символів: +, -, *, /.");
    checkSign();
  }
  return aSign;
};

let add = (x, y) => {
  return x() + y();
};

let subtr = (x, y) => {
  return x() - y();
};

let mult = (x, y) => {
  return x() * y();
};

let div = (x, y) => {
  let a = x();
  let b = y();
  if (b === 0) {
    return "ПОМИЛКА!!! На нуль ділити не можна!";
  }
  return a / b;
};

let calculate = (clbck) => {
  alert(`Результат: ${clbck(checkNum1, checkNum2)}.`);
};

function result(fn) {
  let key = fn();

  switch (key) {
    case "+":
      calculate(add);
      break;
    case "-":
      calculate(subtr);
      break;
    case "*":
      calculate(mult);
      break;
    case "/":
      calculate(div);
      break;
  }
}

result(checkSign);
